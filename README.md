# datex

See the local time in other timezones

## Usage

```
usage: datex [-h] [--version] [--copyright] QUERY [QUERY …]

positional arguments:
  QUERY             timezone or country code

options:
  -h, --help        show this help message and exit
  -V, --version     show version information and exit
  -C, --copyright   show copying policy and exit

Datex will attempt to find valid timezones from either a country code
(e.g., “dk” for “Denmark”) or the name of a region or a city (e.g.,
“Jerusalem”), and then print the current local time in each timezone.
```

## Examples

```sh
$ datex ax dk fi fo gl is no se  # See the time in the Nordic countries
2023-03-14 11:50:14 UTC-0300 (-03)	America/Nuuk
2023-03-14 11:50:14 UTC-0300 (ADT)	America/Thule
2023-03-14 13:50:14 UTC-0100 (-01)	America/Scoresbysund
2023-03-14 14:50:14 UTC+0000 (GMT)	America/Danmarkshavn
2023-03-14 14:50:14 UTC+0000 (GMT)	Atlantic/Reykjavik
2023-03-14 14:50:14 UTC+0000 (WET)	Atlantic/Faroe
2023-03-14 15:50:14 UTC+0100 (CET)	Europe/Copenhagen
2023-03-14 15:50:14 UTC+0100 (CET)	Europe/Oslo
2023-03-14 15:50:14 UTC+0100 (CET)	Europe/Stockholm
2023-03-14 16:50:14 UTC+0200 (EET)	Europe/Helsinki
2023-03-14 16:50:14 UTC+0200 (EET)	Europe/Mariehamn
$ datex nicosia                  # What time is it in Nicosia?
2023-03-14 16:50:17 UTC+0200 (EET)	Asia/Nicosia
2023-03-14 16:50:17 UTC+0200 (EET)	Europe/Nicosia
$ datex 'UTC|GMT'                # See the time in the UTC/GMT timezones
2023-03-14 14:50:19 UTC+0000 (GMT)	GMT
2023-03-14 14:50:19 UTC+0000 (GMT)	GMT+0
2023-03-14 14:50:19 UTC+0000 (GMT)	GMT-0
2023-03-14 14:50:19 UTC+0000 (GMT)	GMT0
2023-03-14 14:50:19 UTC+0000 (UTC)	Etc/UTC
2023-03-14 14:50:19 UTC+0000 (UTC)	UTC
$ # Dates and times will be formatted according to the current locale:
$ LC_ALL=en_US.UTF-8 datex chicago
03/14/2023 09:50:21 AM UTC-0500 (CDT)	America/Chicago
```

For other times than localtime, use e.g. `faketime`:

```sh
$ faketime 12:34:56 datex cn    # Time in China at localtime 12:34:56
2023-03-14 17:34:56 UTC+0600 (+06)	Asia/Urumqi
2023-03-14 19:34:56 UTC+0800 (CST)	Asia/Shanghai
```

## Requirements

Python 3.9+ (only tested on Python 3.10+)

## Installation

Just copy `src/datex` to somewhere in your `$PATH`.

## See also

* [libfaketime](https://github.com/wolfcw/libfaketime "libfaketime modifies the system time for a single application")
* [worldclock](https://github.com/PyBites-Open-Source/pybites-tools "A repo to commit common Python utility scripts and snippets")
